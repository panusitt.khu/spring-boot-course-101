package com.toptier.springboot.course.controller;

import com.toptier.springboot.course.dto.Student;
import com.toptier.springboot.course.service.StudentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log
@RestController
@RequiredArgsConstructor
public class StudentController extends CommonController {

  final StudentService service;

  // create student
//  @ApiParam
  @PostMapping("/students")
  public ResponseEntity<String> createStudent(@RequestBody Student request) {
    return ResponseEntity.ok(service.createStudent(request));
  }

  // find all student
  @GetMapping("/students")
  public ResponseEntity<List<Student>> findAllStudents() {

    log.info("controller find all students");

    return ResponseEntity.ok(service.findAllStudents());
  }

  // find by student code
  @GetMapping("/students/{code}")
  public ResponseEntity<Student> findStudentByCode(@PathVariable String code) {
    return ResponseEntity.ok(service.findStudentByCode(code));
  }

  // update student
  @PatchMapping("/students")
  public ResponseEntity<String> updateStudent(@RequestBody Student request) {
    return ResponseEntity.ok(service.updateStudent(request));
  }

  // delete student
  @DeleteMapping("/students/{code}")
  public ResponseEntity<String> deleteStudent(@PathVariable String code) {
    return ResponseEntity.ok(service.deleteStudent(code));
  }

}
