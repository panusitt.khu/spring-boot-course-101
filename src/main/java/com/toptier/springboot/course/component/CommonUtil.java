package com.toptier.springboot.course.component;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.chrono.ThaiBuddhistChronology;
import java.time.format.DateTimeFormatter;

@Component
public class CommonUtil {

  public String getLocalDateTimeTh() {
    return LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")
            .withChronology(ThaiBuddhistChronology.INSTANCE));
  }

  public String getLocalDateTimeEn() {
    return LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
  }

}
