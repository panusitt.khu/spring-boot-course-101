package com.toptier.springboot.course.dto;

import lombok.Data;

@Data
public class Hello {

  private String name;
  private String tel;
  private String telHome;

}
