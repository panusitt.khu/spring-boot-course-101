package com.toptier.springboot.course.dto.advice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UnAuthException extends RuntimeException {
  private String message;
}
