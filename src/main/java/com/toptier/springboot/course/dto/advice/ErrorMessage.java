package com.toptier.springboot.course.dto.advice;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.OffsetDateTime;

@Data
@AllArgsConstructor
public class ErrorMessage {

  private int status;
  private String message;
  private OffsetDateTime timestamp;
}
