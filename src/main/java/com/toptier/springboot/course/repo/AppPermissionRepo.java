package com.toptier.springboot.course.repo;

import com.toptier.springboot.course.entity.AppPermissionEntity;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AppPermissionRepo extends CrudRepository<AppPermissionEntity, Long> {

  @Query("select a.* from app_permission a where a.app_id=:app_id and a.active = true")
  AppPermissionEntity findOneByAppId(@Param("app_id") String appId);
}
